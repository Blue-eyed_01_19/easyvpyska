﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyVpyska.Entities
{
    public class AdSubscribers
    {
        public int Id { get; set; }      

        public int AdId { get; set; }

        public int? UserId { get; set; }      

        public DateTime Time { get; set; }

        public String Name { get; set; } 
    
    }
}
