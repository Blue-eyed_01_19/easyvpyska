﻿using EasyVpyska.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyVpyska.Repositories.Abstract
{
    public interface IAdRepository
    {
        int CountAdsByType(bool isRequest);

        Ad GetAd(int Id);

        AdAuthor GetAdAuthorById(int id);

        List<Ad> GetAllAdsByType(bool isRequest);

        List<Ad> GetAds(AdFilter filter, int? pageIndex, int? adsPerPage);

        List<Ad> GetAdsMod(AdFilter filter, int StartRowIndex, int MaximumRows);

        int CountSuitableAds(AdFilter filter);

        int CountSuitableAdsMod(AdFilter filter, int StartRowIndex, int MaximumRows);

        int CreateAd(Ad toAdd);

        void UpdateAdStatus(int adId, bool newStatus);

        void UpdateAdActiveStatus(int Id, bool isActive);

        void InsertViews(int UserId, int AdId);

        void DeleteAd(int Id);

        /// <summary>
        /// Adds a comment to a particular advertisment
        /// </summary>
        /// <param name="comment">The comment</param>
        /// <returns>Id of the added comment</returns>
        int AddComment(AdComment comment);

        List<AdComment> GetAdComments(int adId);

        List<AdComment> GetAllAdComments();

        void DeleteAdComment(int Id);

        void UpdateAdComment(int Id, bool IsEnabled);

        bool SubscribeOnAd(int UserId, int AdId);

        List<AdSubscribers> GetAdSubscribers(int adId);

    }
}
