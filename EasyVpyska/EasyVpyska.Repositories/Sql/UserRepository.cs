﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EasyVpyska.Repositories.Abstract;
using System.Data;
using System.Data.SqlClient;
using EasyVpyska.Entities;

namespace EasyVpyska.Repositories.Sql
{
    [System.ComponentModel.DataObject]
    public class UserRepository : IUserRepository
    {
        #region Queries

        private const string UPDATE_USER_QUERY = "UPDATE tblUSER SET Status = @status WHERE Id = @id";
        private const string SELECT_ROLES = "SELECT Role FROM tblRole";
        private const string SELECT_ROLES_FOR_USER = "SELECT DISTINCT r.Role FROM tblRole r JOIN tblUser u ON r.Id = u.Role WHERE u.Login = @Login";
        private const string SELECT_USERS_IN_ROLE = "SELECT u.login FROM tblUser u join tblRole r ON u.Role = r.Id WHERE r.role = @Rolename";
        private const string CHECK_IS_USER_IN_ROLE = "SELECT COUNT(*) FROM tblUser u JOIN tblRole r ON u.Role = r.Id WHERE u.Login = @Username AND r.Role = @Rolename";
        private const string CHECK_IS_USER_ENABLED = " SELECT COUNT(*) FROM [dbo].[tblUser] WHERE Login = @Login AND Status = 1";
        private const string CHECK_IS_ROLE_EXISTS = "SELECT COUNT(*) FROM tblRole WHERE Role = @Rolename";
        private const string SELECT_BIRTH_DATE_BY_ID = "SELECT DateOfBirth FROM tblUser WHERE Id = @id";
        private const string CREATE_USER = "INSERT INTO [dbo].[tblUser] (Login, Password, Role, Firstname, Surname, DateOfBirth, Email, Phone, Country, Town, Address, Status, AboutMyself) VALUES (@Login, @Password, @Role, @Firstname, @Surname, @DateOfBirth, @Email, @Phone, @Country, @Town, @Address, @Status, @AboutMyself);SELECT CAST(scope_identity() AS int)";
        private const string SELECT_ROLE_ID = "SELECT Id FROM [dbo].[tblRole] WHERE Role = @Rolename";
        private const string GET_USER_ID = "SELECT Id FROM [dbo].[tblUser] WHERE Login = @Login";
        private const string GET_USER_HASH = "SELECT Password FROM [dbo].[tblUser] WHERE Login = @Login";
        private const string GET_EMAIL_ID = "SELECT Id FROM [dbo].[tblUser] WHERE Email = @Email";
        private const string GET_PHONE_ID = "SELECT Id FROM [dbo].[tblUser] WHERE Phone = @Phone";
        private const string INSERT_IMAGE = "INSERT INTO [dbo].[tblImage]([FileName], [Image], UserId) values(@FileName, @Image, @UserId);SELECT CAST(scope_identity() AS int)";
        private const string GET_IMAGE = "SELECT Image, FileName, UserId FROM [dbo].[tblImage] where UserId = @UserId";
        private const string CHECK_IS_PROFILE_PHOTO_EXISTS = "SELECT COUNT(*) FROM [dbo].[tblImage] where UserId = @UserId";

        #endregion

        #region Fields

        private readonly string _connectionString;

        #endregion

        #region Constructor

        public UserRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        #endregion

        #region IUserRepository

        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, true)]
        public virtual List<User> SelectAll(int startIndexRow, int maximumRows, string text, int? status)
        {
            List<User> usersTable = new List<User>();
            text = text == null ? string.Empty : text;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspGetUsers", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Filter", text);

                    if (status != null)
                    {
                        command.Parameters.AddWithValue("@Status", status);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@Status", DBNull.Value);
                    }

                    command.Parameters.AddWithValue("@startIndex", startIndexRow);
                    command.Parameters.AddWithValue("@maxRows", maximumRows);

                    SqlParameter TotalRecordSP = new SqlParameter("@TotalRecord", System.Data.SqlDbType.Int);
                    TotalRecordSP.Direction = ParameterDirection.Output;
                    command.Parameters.Add(TotalRecordSP);
                    con.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            User user = new User();

                            user.Id = (int)reader["Id"];
                            user.Login = (string)reader["Login"];
                            user.FirstName = (string)reader["Firstname"];
                            user.SurName = reader["Surname"] is DBNull ? "" : (string)reader["Surname"];
                            user.DateOfBirth = (DateTime)reader["DateOfBirth"];
                            user.Email = (string)reader["Email"];
                            user.Phone = reader["Phone"] is DBNull ? "" : (string)reader["Phone"];
                            user.Country = reader["Country"] is DBNull ? "" : (string)reader["Country"];
                            user.Status = (bool)reader["Status"];
                            user.Town = reader["Town"] is DBNull ? "" : (string)reader["Town"];
                            user.Address = reader["Address"] is DBNull ? "" : (string)reader["Address"];
                            user.About = reader["AboutMyself"] is DBNull ? "" : (string)reader["AboutMyself"];
                            usersTable.Add(user);
                        }
                    }
                }
            }

            return usersTable;
        }

        public int CreateUser(User user, string password)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand commmand = new SqlCommand(CREATE_USER, con))
                {
                    commmand.CommandType = CommandType.Text;

                    commmand.Parameters.AddWithValue("@Login", user.Login);
                    commmand.Parameters.AddWithValue("@Password", password);
                    commmand.Parameters.AddWithValue("@Role", GetRoleId(user.Role));
                    commmand.Parameters.AddWithValue("@Firstname", user.FirstName);
                    commmand.Parameters.AddWithValue("@Surname", user.SurName ?? (object)DBNull.Value);
                    commmand.Parameters.AddWithValue("@DateOfBirth", user.DateOfBirth);
                    commmand.Parameters.AddWithValue("@Email", user.Email);
                    commmand.Parameters.AddWithValue("@Phone", user.Phone ?? (object)DBNull.Value);
                    commmand.Parameters.AddWithValue("@Country", user.Country ?? (object)DBNull.Value);
                    commmand.Parameters.AddWithValue("@Town", user.Town ?? (object)DBNull.Value);
                    commmand.Parameters.AddWithValue("@Address", user.Address ?? (object)DBNull.Value);
                    commmand.Parameters.AddWithValue("@Status", user.Status);
                    commmand.Parameters.AddWithValue("@AboutMyself", user.About ?? (object)DBNull.Value);

                    con.Open();

                    try
                    {
                        return (int)commmand.ExecuteScalar();
                    }
                    catch
                    {
                        return -1;
                    }
                }
            }
        }

        public int GetUserId(string login)
        {
            int id=0;
            if (login != String.Empty)
            {

                using (SqlConnection con = new SqlConnection(_connectionString))
                {
                    using (SqlCommand command = new SqlCommand(GET_USER_ID, con))
                    {
                        command.CommandType = CommandType.Text;

                        command.Parameters.AddWithValue("@Login", login);

                        con.Open();

                        id = (int)command.ExecuteScalar();
                    }
                }
            }

            return id;
        }

        public string GetUserHash(string login)
        {
            var hash = "";

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(GET_USER_HASH, con))
                {
                    command.CommandType = CommandType.Text;

                    command.Parameters.AddWithValue("@Login", login);

                    con.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            hash = (string)reader["Password"];
                        }
                    }
                }
            }

            return hash;
        }


        public User GetUser(string login)
        {
            User user = null;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspGetUser", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@Login", login);

                    con.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            user = new User();

                            user.Id = (int)reader["Id"];
                            user.Login = (string)reader["Login"];
                            user.FirstName = (string)reader["Firstname"];
                            user.Id = (int)reader["Id"];
                            user.Login = (string)reader["Login"];
                            user.FirstName = (string)reader["Firstname"];
                            user.SurName = reader["Surname"] is DBNull ? "" : (string)reader["Surname"];
                            user.DateOfBirth = (DateTime)reader["DateOfBirth"];
                            user.Email = (string)reader["Email"];
                            user.Phone = reader["Phone"] is DBNull ? "" : (string)reader["Phone"];
                            user.Country = reader["Country"] is DBNull ? "" : (string)reader["Country"];
                            user.Status = (bool)reader["Status"];
                            user.Town = reader["Town"] is DBNull ? "" : (string)reader["Town"];
                            user.Address = reader["Address"] is DBNull ? "" : (string)reader["Address"];
                            user.About = reader["AboutMyself"] is DBNull ? "" : (string)reader["AboutMyself"];
                            user.Role = (string)reader["Role"];
                        }
                    }
                }
            }

            return user;
        }

        public bool IsLoginExist(string login)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(GET_USER_ID, con))
                {
                    command.CommandType = CommandType.Text;

                    command.Parameters.AddWithValue("@Login", login);

                    con.Open();

                    if (command.ExecuteScalar() != null)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsEmailExist(string email)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(GET_EMAIL_ID, con))
                {
                    command.CommandType = CommandType.Text;

                    command.Parameters.AddWithValue("@Email", email);

                    con.Open();

                    if (command.ExecuteScalar() != null)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsPhoneExist(string phone)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(GET_PHONE_ID, con))
                {
                    command.CommandType = CommandType.Text;

                    command.Parameters.AddWithValue("@Phone", phone);

                    con.Open();

                    if (command.ExecuteScalar() != null)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public User GetUserById(int id)
        {
            User user = null;
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspGetUserById", con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@id", id);

                    con.Open();

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            user = new User();

                            user.Id = (int)reader["Id"];
                            user.Login = (string)reader["Login"];
                            user.FirstName = (string)reader["Firstname"];
                            user.SurName = reader.IsDBNull(reader.GetOrdinal("Surname")) ? string.Empty : (string)reader["Surname"];
                            user.DateOfBirth = (DateTime)reader["DateOfBirth"];
                            user.Email = (string)reader["Email"];
                            user.Phone = reader.IsDBNull(reader.GetOrdinal("Phone")) ? string.Empty : (string)reader["Phone"];
                            user.Country = reader.IsDBNull(reader.GetOrdinal("Country")) ? "undefined" : (string)reader["Country"];
                            user.Town = reader.IsDBNull(reader.GetOrdinal("Town")) ? "undefined" : (string)reader["Town"];
                            user.Status = (bool)reader["Status"];
                            user.Address = reader.IsDBNull(reader.GetOrdinal("Address")) ? string.Empty : (string)reader["Address"];
                            user.About = reader.IsDBNull(reader.GetOrdinal("AboutMyself")) ? string.Empty : (string)reader["AboutMyself"];

                            user.Role = (string)reader["Role"];
                        }
                    }
                }
            }

            return user;
        }

        public int GetUserAge(int id)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(SELECT_BIRTH_DATE_BY_ID, con) )
                {
                    DateTime birthDate = (DateTime) command.ExecuteScalar();
                    DateTime todayDate = DateTime.Today;
                    TimeSpan timeSpan = (TimeSpan)(todayDate - birthDate);
                    return (int)(timeSpan.TotalDays / 365.0 );
                }
            }
        }

        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Update, true)]
        public virtual void UpdateUser(int Id, bool Status)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(UPDATE_USER_QUERY, con))
                {
                    con.Open();
                    command.Parameters.AddWithValue("@status", Status);
                    command.Parameters.AddWithValue("@id", Id);
                    command.ExecuteNonQuery();
                }
            }
        }

        public string[] GetAllRoles()
        {
            string tmpRoleNames = "";

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(SELECT_ROLES, con))
                {
                    con.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tmpRoleNames += reader.GetString(0) + ",";
                        }
                    }
                }
            }

            if (tmpRoleNames.Length > 0)
            {
                // Remove trailing comma.
                tmpRoleNames = tmpRoleNames.Substring(0, tmpRoleNames.Length - 1);
                return tmpRoleNames.Split(',');
            }

            return new string[0];
        }

        public string[] GetRolesForUser(string login)
        {
            string tmpRoleNames = "";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(SELECT_ROLES_FOR_USER, conn))
                {
                    cmd.Parameters.AddWithValue("@Login", login);

                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            tmpRoleNames += reader.GetString(0) + ",";
                        }
                    }
                }
            }

            if (tmpRoleNames.Length > 0)
            {
                // Remove trailing comma.
                tmpRoleNames = tmpRoleNames.Substring(0, tmpRoleNames.Length - 1);
                return tmpRoleNames.Split(',');
            }

            return new string[0];
        }

        public string[] GetUsersInRole(string rolename)
        {
            string tmpUserNames = "";

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(SELECT_USERS_IN_ROLE))
                {
                    cmd.Parameters.AddWithValue("Rolename", rolename);

                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        while (reader.Read())
                        {
                            tmpUserNames += reader.GetString(0) + ",";
                        }
                    }
                }
            }

            if (tmpUserNames.Length > 0)
            {
                // Remove trailing comma.
                tmpUserNames = tmpUserNames.Substring(0, tmpUserNames.Length - 1);
                return tmpUserNames.Split(',');
            }

            return new string[0];
        }

        public int GetRoleId(string rolename)
        {
            int id;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(SELECT_ROLE_ID, con))
                {
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.AddWithValue("@Rolename", rolename);

                    con.Open();

                    id = (int)cmd.ExecuteScalar();
                }
            }

            return id;
        }

        public bool IsUserInRole(string login, string rolename)
        {
            bool userIsInRole = false;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(CHECK_IS_USER_IN_ROLE, conn))
                {
                    cmd.Parameters.AddWithValue("@Username", login);
                    cmd.Parameters.AddWithValue("@Rolename", rolename);

                    conn.Open();

                    int numRecs = (int)cmd.ExecuteScalar();

                    if (numRecs > 0)
                    {
                        userIsInRole = true;
                    }
                }
            }

            return userIsInRole;
        }

        public bool IsUserEnabled(string login)
        {
            bool userIsEnabled = false;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(CHECK_IS_USER_ENABLED, conn))
                {
                    cmd.Parameters.AddWithValue("@Login", login);

                    conn.Open();

                    int numRecs = (int)cmd.ExecuteScalar();

                    if (numRecs > 0)
                    {
                        userIsEnabled = true;
                    }
                }
            }

            return userIsEnabled;
        }


        public bool IsRoleExists(string rolename)
        {
            bool exists = false;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(CHECK_IS_ROLE_EXISTS, conn))
                {

                    cmd.Parameters.AddWithValue("@Rolename", rolename);

                    conn.Open();

                    int numRecs = (int)cmd.ExecuteScalar();

                    if (numRecs > 0)
                    {
                        exists = true;
                    }
                }
            }

            return exists;
        }

        public int UpdateUser(User user, string password)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("uspUpdateUser", conn))
                {

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@Id", user.Id);

                    password = password == "" ? null : password;
                    user.Country = user.Country == "" ? null : user.Country;
                    user.Town = user.Town == "" ? null : user.Town;
                    user.Address = user.Address.Trim(' ') == "" ? null : user.Address;

                    cmd.Parameters.AddWithValue("@Password", password ?? (object)DBNull.Value);     
                    cmd.Parameters.AddWithValue("@Firstname", user.FirstName ?? (object)DBNull.Value);     
                    cmd.Parameters.AddWithValue("@Surname", user.SurName ?? (object)DBNull.Value);     
                    cmd.Parameters.AddWithValue("@DateOfBirth", user.DateOfBirth);     
                    cmd.Parameters.AddWithValue("@Email", user.Email ?? (object)DBNull.Value);     
                    cmd.Parameters.AddWithValue("@Phone", user.Phone ?? (object)DBNull.Value);     
                    cmd.Parameters.AddWithValue("@Country", user.Country ?? (object)DBNull.Value);     
                    cmd.Parameters.AddWithValue("@Town", user.Town ?? (object)DBNull.Value);     
                    cmd.Parameters.AddWithValue("@Address", user.Address ?? (object)DBNull.Value);     
                    cmd.Parameters.AddWithValue("@AboutMyself", user.About ?? (object)DBNull.Value);

                    conn.Open();

                    return cmd.ExecuteNonQuery();
                }
            }
        }

        public int UpdatePhoto(ProfileImage image)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("uspUpdateImage", conn))
                {

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@UserId",image.UserId);
                    cmd.Parameters.AddWithValue("@FileName", image.FileName ?? (object)DBNull.Value);
                    cmd.Parameters.AddWithValue("@Image", image.File ?? (object)DBNull.Value);

                    conn.Open();

                    return cmd.ExecuteNonQuery();
                }
            }
        }

        public int UploadPhoto(ProfileImage image)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand commmand = new SqlCommand(INSERT_IMAGE, con))
                {
                    commmand.Parameters.AddWithValue("@FileName", image.FileName);
                    commmand.Parameters.AddWithValue("@Image", image.File);
                    commmand.Parameters.AddWithValue("@UserId", image.UserId);

                    con.Open();

                    try
                    {
                        return (int)commmand.ExecuteScalar();
                    }
                    catch
                    {
                        return -1;
                    }
                }
            }
        }

        public ProfileImage LoadPhoto(int userId)
        {
            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand commmand = new SqlCommand(GET_IMAGE, con))
                {

                    ProfileImage image = new ProfileImage();

                    commmand.Parameters.AddWithValue("@UserId", userId);

                    con.Open();

                    var reader = commmand.ExecuteReader();

                    if (reader.Read())
                    {
                        image.FileName = (string)reader["FileName"];
                        image.File = (byte[])reader["Image"];
                        image.UserId = (int)(reader["UserId"]);
                    }

                    return image;
                }
            }
        }

        public bool IsProfilePhotoExists(int userId)
        {
            bool exists = false;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                using (SqlCommand cmd = new SqlCommand(CHECK_IS_PROFILE_PHOTO_EXISTS, conn))
                {
                    cmd.Parameters.AddWithValue("@UserId", userId);

                    conn.Open();

                    int numRecs = (int)cmd.ExecuteScalar();

                    if (numRecs > 0)
                    {
                        exists = true;
                    }
                }
            }

            return exists;
        }


        #endregion

        #region Helpers

        public int GetCount(string text, int? status)
        {
            int intCount = 0;
            text = text == null ? string.Empty : text;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand("uspGetUsersCount", con))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Filter", text);

                    if (status != null)
                    {
                        command.Parameters.AddWithValue("@Status", status);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@Status", DBNull.Value);
                    }
                    con.Open();

                    intCount = (int)command.ExecuteScalar();
                }
            }

            return intCount;
        }

        public int GetUsersCount()
        {
            string strSql = "SELECT COUNT(*) FROM tblUser";

            int intCount = 0;

            using (SqlConnection con = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(strSql, con))
                {
                    con.Open();

                    intCount = (int)command.ExecuteScalar();
                }
            }

            return intCount;
        }

        #endregion
    }
}
