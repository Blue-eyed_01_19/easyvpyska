USE [EasyVpyska];

GO

CREATE TABLE tblRole
( 
	Id           INT IDENTITY(1, 1) NOT NULL,
	Role         VARCHAR(32) NOT NULL UNIQUE,
	Description  NVARCHAR(256),

	CONSTRAINT pk_Role_Id PRIMARY KEY (Id)
); 

GO

CREATE TABLE tblUser 
( 
	Id           INT IDENTITY(1, 1) NOT NULL,
	Login        VARCHAR(256) NOT NULL UNIQUE,
	Password     VARCHAR(64) NOT NULL,
	Role		 INT NOT NULL,
	Firstname    NVARCHAR(64) NOT NULL,
	Surname      NVARCHAR(64) NULL,
	DateOfBirth  DATETIME  NOT NULL,
	Email        NVARCHAR(256) NOT NULL UNIQUE,
	Phone        NVARCHAR(64) NULL,
	Country      NVARCHAR(64) NULL,
	Town         NVARCHAR(64) NULL,
	Address      NVARCHAR(64) NULL,
	Status       BIT NOT NULL,
	AboutMyself  NVARCHAR(256),

	CONSTRAINT pk_User_Id PRIMARY KEY (ID),
	CONSTRAINT fk_Role FOREIGN KEY (Role) REFERENCES tblRole (Id),
	CONSTRAINT chk_Email CHECK (Email LIKE '%@%.%'),
	CONSTRAINT chk_Date_of_birth CHECK (DateOfBirth > '1880-01-01'),
	CONSTRAINT chk_Phone CHECK (Phone LIKE '+[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' OR Phone=NULL)
); 

GO

CREATE TABLE tblAd
(
	Id			INT IDENTITY(1, 1) NOT NULL,
	IsRequest	BIT NOT NULL,
	IsEnabled	BIT NOT NULL,
	IsActive    BIT NOT NULL,
	Title		NVARCHAR(64) NOT NULL,
	AuthorId	INT NOT NULL,
	Description NVARCHAR(256) NOT NULL,
	Begin_date	DATETIME NOT NULL,
	End_date	DATETIME NOT NULL,
	Country		NVARCHAR(64) NOT NULL,
	Town		NVARCHAR(64) NOT NULL,	
	Address		NVARCHAR(64),
	
	
	CONSTRAINT pk_Ad_Id PRIMARY KEY (Id),
	CONSTRAINT fk_User_id FOREIGN KEY (AuthorId) REFERENCES tblUser (Id),
	CONSTRAINT chk_Date_of_start CHECK (Begin_date > '2015-01-01'),
	CONSTRAINT chk_Date_of_end CHECK (End_date > '2015-01-01')	
);

GO

select * from tblAd

CREATE TABLE tblAdComment
(
	Id INT IDENTITY(1, 1) NOT NULL,
	IsEnabled BIT NOT NULL,
	AdId INT NOT NULL,
	UserId INT NULL,
	[Time] DATETIME NOT NULL,
	[Comment] NVARCHAR(max) NOT NULL,
	CONSTRAINT pk_AdComment_Id PRIMARY KEY (Id),
	CONSTRAINT fk_AdComment_AdId_Ad_id FOREIGN KEY (AdId) REFERENCES tblAd (Id) ON DELETE CASCADE ,
	CONSTRAINT fk_AdComment_UserId_User_id FOREIGN KEY (UserId) REFERENCES tblUser (Id)

);


GO

CREATE TABLE tblImage
(
	Id INT IDENTITY(1, 1) NOT NULL,
	[FileName] NVARCHAR(128) NULL,
	[Image]  VARBINARY(MAX) NULL,
	UserId INT NOT NULL,
	CONSTRAINT pk_tblImage_Id PRIMARY KEY (Id),
	CONSTRAINT fk_tblImage_UserId FOREIGN KEY (UserId) REFERENCES tblUser (Id)
);

GO

CREATE TABLE tblAdViewers
(
	Id INT IDENTITY(1, 1) NOT NULL,	
	AdId INT NOT NULL,
	UserId INT NOT NULL

	CONSTRAINT uc_ViewId UNIQUE (AdId,UserId),
	CONSTRAINT pk_AdViewers_Id PRIMARY KEY (Id),
	CONSTRAINT fk_AdViewers_AdId_Ad_id FOREIGN KEY (AdId) REFERENCES tblAd (Id) ON DELETE CASCADE ,
	CONSTRAINT fk_AdViewers_UserId_User_id FOREIGN KEY (UserId) REFERENCES tblUser (Id)

);

GO

CREATE TABLE tblAdSubscribers
(
	Id INT IDENTITY(1, 1) NOT NULL,	
	AdId INT NOT NULL,
	UserId INT NOT NULL,
	[Date] DATETIME NOT NULL,

	CONSTRAINT uc_SubscriberId UNIQUE (AdId,UserId),
	CONSTRAINT pk_AdSubscribers_Id PRIMARY KEY (Id),
	CONSTRAINT fk_AdSubscribers_AdId_Ad_id FOREIGN KEY (AdId) REFERENCES tblAd (Id) ON DELETE CASCADE ,
	CONSTRAINT fk_AdSubscribers_UserId_User_id FOREIGN KEY (UserId) REFERENCES tblUser (Id)

);