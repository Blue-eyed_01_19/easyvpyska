USE [EasyVpyska];

GO

DROP TABLE [dbo].[tblAdComment];

GO

DROP TABLE [dbo].[tblAdViewers];

GO

DROP TABLE [dbo].[tblAdSubscribers];

GO

DROP TABLE [dbo].[tblImage];

GO

DROP TABLE [dbo].[tblAd];

GO

DROP TABLE [dbo].[tblUser];

GO

DROP TABLE [dbo].[tblRole];



GO

DROP PROCEDURE [dbo].[uspGetUsers];

GO

DROP PROCEDURE [dbo].[uspGetUser];

GO

DROP PROCEDURE [dbo].[uspGetUsersCount];

GO

DROP PROCEDURE [dbo].[uspGetUserById];

GO

DROP PROCEDURE [dbo].[uspGetAdAuthorById];

GO

DROP PROCEDURE [dbo].[uspGetAdsByFilter];

GO

DROP PROCEDURE [dbo].[uspGetCommentsByAdId];

GO

DROP PROCEDURE [dbo].[uspGetAllComments];

GO

DROP PROCEDURE [dbo].[uspUpdateAdActiveStatus];

GO


DROP PROCEDURE [dbo].[uspGetAds];

GO

DROP PROCEDURE [dbo].[uspCountSuitableAds];

GO

DROP PROCEDURE [dbo].[uspGetAdById];

GO

DROP PROCEDURE [dbo].[uspUpdateAdEnableStatus];

GO

DROP PROCEDURE [dbo].[uspUpdateAdCommentStatus];

GO

DROP PROCEDURE [dbo].[uspUpdateUser]

GO

DROP PROCEDURE [dbo].[uspUpdateImage]

GO

DROP PROCEDURE [dbo].[uspCountAdViewers]

GO


DROP PROCEDURE [dbo].[uspGetSubscribersByAdId]
GO