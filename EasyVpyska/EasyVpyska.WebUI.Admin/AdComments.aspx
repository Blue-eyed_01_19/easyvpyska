﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAdminPage.Master" AutoEventWireup="true" CodeBehind="AdComments.aspx.cs" Inherits="EasyVpyska.WebUI.Admin.AdComments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="headTitle" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="filter" runat="server">
    <asp:GridView ID="adcommentsGridView" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="commentsODS">
        <Columns>
            <asp:CommandField ShowEditButton="True" />
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
            <asp:CheckBoxField DataField="IsEnabled" HeaderText="IsEnabled" SortExpression="IsEnabled" />
            <asp:BoundField DataField="AdId" HeaderText="AdId" ReadOnly="True" SortExpression="AdId" />
            <asp:BoundField DataField="UserId" HeaderText="UserId" ReadOnly="True" SortExpression="UserId" />
            <asp:BoundField DataField="UserName" HeaderText="UserName" ReadOnly="True" SortExpression="UserName" />
            <asp:BoundField DataField="Time" HeaderText="Time" ReadOnly="True" SortExpression="Time" />
            <asp:BoundField DataField="Comment" HeaderText="Comment" ReadOnly="True" SortExpression="Comment" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="commentsODS" runat="server" OnObjectCreating="commentsODS_ObjectCreating" OnSelecting="commentsODS_Selecting" SelectMethod="GetAdComments" TypeName="EasyVpyska.Repositories.Sql.AdRepository" UpdateMethod="UpdateAdComment">
        <SelectParameters>
            <asp:Parameter Name="adId" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="Id" Type="Int32" />
            <asp:Parameter Name="IsEnabled" Type="Boolean" />
        </UpdateParameters>
    </asp:ObjectDataSource>
    <asp:Button ID="ReturnToAdsBtn" runat="server" OnClick="ReturnToAdsBtn_Click" Text="Return to Ads Table" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">
</asp:Content>
