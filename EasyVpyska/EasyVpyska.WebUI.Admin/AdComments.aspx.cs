﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.SessionState;
using EasyVpyska.Repositories;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.Repositories.Sql;
using EasyVpyska.Entities;
using EasyVpyska.WebUI.Admin.Code.Keys;
using EasyVpyska.WebUI.Admin.Code.Models;
using Ninject;

using EasyVpyska.WebUI.Admin.Code;

namespace EasyVpyska.WebUI.Admin
{
    public partial class AdComments : System.Web.UI.Page
    {
        private int AdId
        {
            get
            {
                Session[SessionKeys.AD_ID] = Session[SessionKeys.AD_ID];
                return (int)Session[SessionKeys.AD_ID];
            }
            set
            {
                Session[SessionKeys.AD_ID] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            AdId = Convert.ToInt32(Request.QueryString["AdId"]);
            Page.Title = "Comments for ad #" + AdId.ToString() + "- Administrate";
        }

        #region ControlsEvents

        protected void commentsODS_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = ServiceLocator.Kernel.Get<IAdRepository>();
        }

        protected void commentsODS_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
        {
            e.InputParameters.Remove("adId");
            e.InputParameters.Add("adId", AdId);
        }

        protected void ReturnToAdsBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Ads.aspx");
        }

        #endregion
    }
}