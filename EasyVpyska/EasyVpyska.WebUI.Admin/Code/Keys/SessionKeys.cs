﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyVpyska.WebUI.Admin.Code.Keys
{
    public static class SessionKeys
    {
        public const string FILTER = "Filter";

        public const string AD_FILTER = "Ad_Filter";

        public const string AD_ID = "Ad_Id";
    }
}