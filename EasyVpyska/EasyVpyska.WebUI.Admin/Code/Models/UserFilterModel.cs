﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyVpyska.WebUI.Admin.Code.Models
{
    public class UserFilterModel
    {
        #region Properties

        public string Text { get;  set; }
        public int? Status { get; set; }

        #endregion
    }
}