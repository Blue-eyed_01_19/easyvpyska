﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" MasterPageFile="~/MasterAdminPage.Master" Inherits="EasyVpyska.WebUI.Admin.LoginPage" %>

<asp:Content ID="pageHead" ContentPlaceHolderID="headTitle" runat="server">
    <h1>Welcome to Admin page</h1>
    </asp:Content>

<asp:Content ID="StartContent" ContentPlaceHolderID="content" runat="server">
    <div class="LogIn">
        <asp:Label ID="lblLogin" runat="server" Text="Login"></asp:Label>
        <div class="marg">
        <asp:TextBox ID="txtLogin" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ValidationGroup="LoginPassVallidationGroup" ID="LoginRequiredFieldValidator" runat="server" ErrorMessage="Login required" ControlToValidate="txtLogin" ForeColor="Red" />
        </div>
        <div class="marg">
        <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
        </div>
        <div class="marg">
        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ValidationGroup="LoginPassVallidationGroup" ID="PasswordFieldValidator" runat="server" ErrorMessage="Password required" ForeColor="Red" ControlToValidate="txtPassword" />
        </div>
        <div class="marg">
        <asp:Label ID="LegendStatus" runat="server" ForeColor="Red" EnableViewState="false" Text="" />
        
        </div>
        <asp:Button ID="btnLogIn" runat="server"  ValidationGroup="LoginPassVallidationGroup" Text="Enter" OnClick="btnLogIn_Click" CausesValidation="true" />
    </div>
</asp:Content>
