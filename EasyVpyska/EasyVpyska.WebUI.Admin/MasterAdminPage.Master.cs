﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Configuration;
using Ninject;


using EasyVpyska.WebUI;
using EasyVpyska.WebUI.Abstract;

using EasyVpyska.WebUI.Admin.Code;

namespace EasyVpyska.WebUI.Admin
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        private ISecurityManager _securityManager;// = new StandardKernel(new MyConfigModule()).Get<ISecurityManager>();


        protected void Page_Load(object sender, EventArgs e)
        {
            _securityManager = ServiceLocator.Kernel.Get<ISecurityManager>();
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            //SecurityManager securityManager = new
            //    SecurityManager(ConfigurationManager.ConnectionStrings["ConnectToSql"].ConnectionString);

            if (!_securityManager.IsCurrentUserAuthenticated())
            {
                btnSignOut.Visible = false;
                btnAds.Visible = false;
                btnUsers.Visible = false;
                btnComments.Visible = false;
            }
        }

        protected void btnSignOut_Click(object sender, EventArgs e)
        {
            //SecurityManager securityManager = new SecurityManager(
            //   ConfigurationManager.ConnectionStrings["ConnectToSql"].ConnectionString);

            _securityManager.SignOut();
            _securityManager.RedirectToLoginPage();            
        }

        protected void btnUsers_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Users.aspx");
        }

        protected void btnAds_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Ads.aspx");
        }


        protected void btnComments_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Comments.aspx");
        }
        
        
    }
}