﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Users.aspx.cs" MasterPageFile="~/MasterAdminPage.Master" Inherits="EasyVpyska.WebUI.Admin.AdminPage" %>

<asp:Content ID="pageHead" ContentPlaceHolderID="headTitle" runat="server">
    <h1>Administrate site users</h1>
    </asp:Content>


<asp:Content ID="StartContent" ContentPlaceHolderID="content" runat="server">
     <asp:GridView ID="GridViewUsers"
        runat="server"
        SkinID="UserTable"
        AllowPaging="True"
        PageSize="5"
        AutoGenerateColumns="False"
        AutoGenerateEditButton="True"
        DataKeyNames="Id"
        DataSourceID="odsUsers" 
        BackColor="#CCCCCC" 
        BorderColor="#CCCCCC"
        BorderStyle="Solid" 
        BorderWidth="3px" 
        CellPadding="4" 
        ForeColor="Black" 
        CellSpacing="2">
        <Columns>            
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" />
            <asp:BoundField DataField="Login" HeaderText="Login" ReadOnly="True" />
            <asp:CheckBoxField DataField="Status" HeaderText="Status" />
            <asp:BoundField DataField="Firstname" HeaderText="First Name" ReadOnly="True" />
            <asp:BoundField DataField="Surname" HeaderText="Surname" ReadOnly="True" />
            <asp:BoundField DataField="DateOfBirth" HeaderText="Birthdate" DataFormatString="{0:dd/MM/yy }" ReadOnly="True" />
            <asp:BoundField DataField="Email" HeaderText="Email" ReadOnly="True" />
            <asp:BoundField DataField="Phone" HeaderText="Phone" ReadOnly="True" />
            <asp:BoundField DataField="Country" HeaderText="Country" ReadOnly="True" />
            <asp:BoundField DataField="Town" HeaderText="Town" ReadOnly="True" />
            <asp:BoundField DataField="Address" HeaderText="Address" ReadOnly="True" />
            <asp:BoundField DataField="About" HeaderText="User Information" ReadOnly="True" />
        </Columns>
        <FooterStyle BackColor="#CCCCCC" Height="24px"/>
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White"/>
        <PagerStyle HorizontalAlign="Left" CssClass="pager"/>
        <RowStyle BackColor="White" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>
    <asp:ObjectDataSource
        ID="odsUsers"
        runat="server"
        SelectCountMethod="GetCount"
        EnablePaging="True"
        TypeName="EasyVpyska.Repositories.Sql.UserRepository"
        SelectMethod="SelectAll"
        UpdateMethod="UpdateUser"
        MaximumRowsParameterName="maximumRows"
        StartRowIndexParameterName="startIndexRow"
        OnObjectCreating="ODS_GridViewUsers_ObjectCreating"
        OnSelecting="ODS_UsersGrid_Selecting"
        OldValuesParameterFormatString="{0}">
        <SelectParameters>
            <asp:Parameter Name="text" Type="String" DefaultValue="" />
            <asp:Parameter Name="status" Type="String" DefaultValue="" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>

<asp:Content ID="FilterBar" ContentPlaceHolderID="filter" runat="server">
       <div class="info">
        <p>
        <span><strong>Date:</strong></span>
        <asp:Label ID="lblDate" runat="server" Text=""></asp:Label>
         </p>
         <p>
         <span><strong>Time:</strong></span>
        <asp:Label ID="lblTime" runat="server" Text=""></asp:Label>
         </p>
        <span><strong>Registrated users:</strong></span>
        <asp:Label ID="lblUsers" runat="server" Text=""></asp:Label>
     </div>
      
    <div class="search">
     <asp:Label ID="Label1" runat="server" Text="Search word:"></asp:Label>
    <asp:TextBox ID="tbSearchWord" runat="server" SkinId="FilterTxtbox"></asp:TextBox>
    <asp:Button ID="btnSearch" OnClick="btnSearch_Click" runat="server" SkinID="FilterButton" Text="Search" />
    <asp:Label ID="Label2" runat="server" Text="Status:"></asp:Label>

    <asp:DropDownList ID="Status" runat="server" OnSelectedIndexChanged="Status_SelectedIndexChanged" AutoPostBack="True">
        <asp:ListItem runat="server">All</asp:ListItem>
        <asp:ListItem runat="server">Enabled</asp:ListItem>
        <asp:ListItem runat="server">Disabled</asp:ListItem>
    </asp:DropDownList>

    </div>
</asp:Content>
