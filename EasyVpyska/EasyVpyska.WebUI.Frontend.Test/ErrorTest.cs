﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.Entities;
using EasyVpyska.WebUI.Frontend.Controllers;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;
using System.IO;

namespace EasyVpyska.WebUI.Frontend.Test
{
    [TestClass]
    public class ErrorTest
    {
        [TestMethod]
        public void Error404Test()
        {   
            var request= new Mock<IRequestService>();

            request.Setup(a => a.ReturnPath(It.IsAny<Controller>())).Returns("/123");
            request.Setup(a => a.CheckAjaxRequest(It.IsAny<Controller>())).Returns(true);

            ErrorsController controller = new ErrorsController(request.Object);
                        
            // Act
            ViewResult result = controller.Error404() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Error404SecondTest()
        {
            var request = new Mock<IRequestService>();

            request.Setup(a => a.ReturnPath(It.IsAny<Controller>())).Returns("/123");
            request.Setup(a => a.CheckAjaxRequest(It.IsAny<Controller>())).Returns(false);

            ErrorsController controller = new ErrorsController(request.Object);

            // Act
            ViewResult result = controller.Error404() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Error500Test()
        {
            var request = new Mock<IRequestService>();

            request.Setup(a => a.ReturnPath(It.IsAny<Controller>())).Returns("/123");
            request.Setup(a => a.CheckAjaxRequest(It.IsAny<Controller>())).Returns(true);

            ErrorsController controller = new ErrorsController(request.Object);

            // Act
            ViewResult result = controller.Error500() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Error500SecondTest()
        {
            var request = new Mock<IRequestService>();

            request.Setup(a => a.ReturnPath(It.IsAny<Controller>())).Returns("/123");
            request.Setup(a => a.CheckAjaxRequest(It.IsAny<Controller>())).Returns(false);

            ErrorsController controller = new ErrorsController(request.Object);

            // Act
            ViewResult result = controller.Error500() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
