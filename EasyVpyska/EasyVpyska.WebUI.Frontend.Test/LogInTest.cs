﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.WebUI.Frontend.Controllers;
using System.Web.Mvc;

namespace EasyVpyska.WebUI.Frontend.Test
{
    [TestClass]
    public class LogInTest
    {
        [TestMethod]
        public void LogInAuthTest()
        {
            var securityMock = new Mock<ISecurityManager>();
            var email = new Mock<IEmailService>();

            securityMock.Setup(a => a.Authentication(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            securityMock.Setup(a => a.IsUserEnabled(It.IsAny<string>())).Returns(true);


            LogInController controller = new LogInController(securityMock.Object, email.Object);
            ContentResult result = controller.LogIn("123", "321") as ContentResult;

            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void LogInEnableTest()
        {
            var securityMock = new Mock<ISecurityManager>();
            var email = new Mock<IEmailService>();

            securityMock.Setup(a => a.Authentication(It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            securityMock.Setup(a => a.IsUserEnabled(It.IsAny<string>())).Returns(false);

            LogInController controller = new LogInController(securityMock.Object, email.Object);
            ContentResult result = controller.LogIn("123", "321") as ContentResult;

            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void LogInAuthFailTest()
        {
            var securityMock = new Mock<ISecurityManager>();
            var email = new Mock<IEmailService>();

            securityMock.Setup(a => a.Authentication(It.IsAny<string>(), It.IsAny<string>())).Returns(false);
            LogInController controller = new LogInController(securityMock.Object, email.Object);

            ContentResult result = controller.LogIn("123", "321") as ContentResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void LogOutAuthTest()
        {
            var securityMock = new Mock<ISecurityManager>();
            var email = new Mock<IEmailService>();

            securityMock.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            LogInController controller = new LogInController(securityMock.Object, email.Object);

            JsonResult result = controller.LogOut() as JsonResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void LogOutTest()
        {
            var securityMock = new Mock<ISecurityManager>();
            var email = new Mock<IEmailService>();

            securityMock.Setup(a => a.IsCurrentUserAuthenticated()).Returns(false);
            LogInController controller = new LogInController(securityMock.Object, email.Object);

            JsonResult result = controller.LogOut() as JsonResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void IsCurrentUserSignedTest()
        {
            var securityMock = new Mock<ISecurityManager>();
            var email = new Mock<IEmailService>();

            securityMock.Setup(a => a.IsCurrentUserAuthenticated()).Returns(true);
            LogInController controller = new LogInController(securityMock.Object, email.Object);

            JsonResult result = controller.IsCurrentUserSigned() as JsonResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void IsCurrentUserSignedFailTest()
        {
            var securityMock = new Mock<ISecurityManager>();
            var email = new Mock<IEmailService>();

            securityMock.Setup(a => a.IsCurrentUserAuthenticated()).Returns(false);
            LogInController controller = new LogInController(securityMock.Object, email.Object);

            JsonResult result = controller.IsCurrentUserSigned() as JsonResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void SendMailToAdminTest()
        {
            var securityMock = new Mock<ISecurityManager>();
            var email = new Mock<IEmailService>();         

            LogInController controller = new LogInController(securityMock.Object, email.Object);

            JsonResult result = controller.SendMailToAdmin("message") as JsonResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void SendMailToAdminExceptionTest()
        {
            var securityMock = new Mock<ISecurityManager>();
            var email = new Mock<IEmailService>();

            email.Setup(a => a.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException("command"));
            LogInController controller = new LogInController(securityMock.Object, email.Object);

            JsonResult result = controller.SendMailToAdmin("message") as JsonResult;

            Assert.IsNotNull(result);
        }

    }
}
