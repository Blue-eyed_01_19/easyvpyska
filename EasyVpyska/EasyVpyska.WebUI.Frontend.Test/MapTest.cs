﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.Entities;
using EasyVpyska.WebUI.Frontend.Controllers;
using System.Web.Mvc;
using System.Collections.Generic;

namespace EasyVpyska.WebUI.Frontend.Test
{
    [TestClass]
    public class MapTest
    {
        Mock<IAdRepository> adMock = new Mock<IAdRepository>();

        [TestMethod]
        public void IndexTest()
        {                        
            MapController controller = new MapController(adMock.Object);

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }
    

            [TestMethod]
        public void GetAddressesTest()
        {    
            MapController controller = new MapController(adMock.Object);
            Ad ad = new Ad();
            ad.Id = 1;
            ad.Town = "Lviv";
            ad.Country = "Ukraine";
            ad.Address = "Zelena, 5";

            adMock.Setup(a => a.GetAllAdsByType(false)).Returns(new List<Ad>() { ad});
            JsonResult result = controller.GetAddresses() as JsonResult;

            Assert.IsNotNull(result);
        }

            [TestMethod]
            public void GetAdFullInfoTest()
            {                
                AdAuthor author=new AdAuthor();
                author.FirstName="Fill";

                adMock.Setup(a => a.GetAd(It.IsAny<int>())).Returns(new Ad() { Author = author });              
                MapController controller = new MapController(adMock.Object);

                JsonResult result = controller.GetAdFullInfo(1) as JsonResult;

                Assert.IsNotNull(result);
            }



    }
}

