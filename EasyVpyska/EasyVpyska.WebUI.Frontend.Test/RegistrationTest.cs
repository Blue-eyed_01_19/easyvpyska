﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Collections.Generic;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using EasyVpyska.Repositories.Abstract;
using EasyVpyska.Entities;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.WebUI.Frontend.Controllers;

namespace EasyVpyska.WebUI.Frontend.Test
{
    /// <summary>
    /// Summary description for RegistrationTest
    /// </summary>
    /// 
    [TestClass]
    public class RegistrationTest
    {
        Mock<IUserRepository> userMock = new Mock<IUserRepository>();
        Mock<ISecurityManager> securityMock = new Mock<ISecurityManager>();
        Mock<IImageService> imageMock = new Mock<IImageService>();

        [TestMethod]
        public void RegistrationInexTest()
        {
            var controler = new RegistrationController(securityMock.Object, userMock.Object, imageMock.Object);

            var view = controler.Index();

            Assert.IsNotNull(view);
        }

        [TestMethod]
        public void RegistrateTest()
        {
            var photo = new Mock<HttpPostedFileBase>();
            var stream = new Mock<Stream>();

            photo.Setup(a => a.InputStream).Returns(stream.Object);

            User userTest = new User();
            userTest.Id = 1;
            string userJson = "{\"FirstName\":\"Petr\",\"SurName\":\"Voycikhovsky\",\"DateOfBirth\":\"01/13/1958\",\"Email\":\"voycikhovsky@mail.ru\",\"Phone\":\"+380995156814\",\"Country\":\"Ukraine\",\"Town\":\"Vinnica\",\"Address\":\"Наливайченка 3\",\"About\":\"Не хроплю!\"}";

            securityMock.Setup(a => a.RegistrateUser(userTest, It.IsAny<string>())).Returns(1);
            userMock.Setup(a => a.LoadPhoto(It.Is<int>(i => i > 0))).Returns(new EasyVpyska.Entities.ProfileImage() { File = null });

            var controler = new RegistrationController(securityMock.Object, userMock.Object, imageMock.Object);

            var result = controler.Registrate(userJson, "123456789", "123456789", photo.Object, "");

            // Assert           
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RegistrateErrorTest()
        {
            var photo = new Mock<HttpPostedFileBase>();
            var stream = new Mock<Stream>();

            photo.Setup(a => a.InputStream).Returns(stream.Object);

            User userTest = new User();
            userTest.Id = 1;
            string userJson = "{\"FirstName\":\"\",\"SurName\":\"\",\"DateOfBirth\":\"\",\"Email\":\"\",\"Phone\":\"\",\"Country\":\"\",\"Town\":\"\",\"Address\":\"\",\"About\":\"\"}";

            securityMock.Setup(a => a.RegistrateUser(userTest, It.IsAny<string>())).Returns(1);
            userMock.Setup(a => a.LoadPhoto(It.Is<int>(i => i > 0))).Returns(new EasyVpyska.Entities.ProfileImage() { File = null });

            var controler = new RegistrationController(securityMock.Object, userMock.Object, imageMock.Object);

            var result = controler.Registrate(userJson, "123456789", "123456789", photo.Object, "");

            // Assert           
            Assert.IsNotNull(result);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsEmailAwaliableErrorTest()
        {
            EditController controller = new EditController(userMock.Object, securityMock.Object, imageMock.Object);

            // Act
            var result = controller.IsEmailAwaliable(null) as JsonResult;
        }

        [TestMethod]
        public void IsEmailAwaliableTest()
        {
            User user = new User();
            user.Email = "email";
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(user);

            EditController controller = new EditController(userMock.Object, securityMock.Object, imageMock.Object);

            // Act
            var result = controller.IsEmailAwaliable("email") as JsonResult;

            // Assert           
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void IsEmailAwaliableSecondTest()
        {
            User user = new User();
            user.Email = "14";
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(user);
            userMock.Setup(a => a.IsEmailExist(It.IsAny<string>())).Returns(true);

            EditController controller = new EditController(userMock.Object, securityMock.Object, imageMock.Object);

            // Act
            var result = controller.IsEmailAwaliable("email") as JsonResult;

            // Assert           
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void IsEmailAwaliableThirdTest()
        {
            User user = new User();
            user.Email = "m";
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(user);
            userMock.Setup(a => a.IsEmailExist(It.IsAny<string>())).Returns(false);

            EditController controller = new EditController(userMock.Object, securityMock.Object, imageMock.Object);

            // Act
            var result = controller.IsEmailAwaliable("email") as JsonResult;

            // Assert           
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IsPhoneAwaliableErrorTest()
        {
            EditController controller = new EditController(userMock.Object, securityMock.Object, imageMock.Object);

            // Act
            var result = controller.IsPhoneAwaliable(null) as JsonResult;
        }

        [TestMethod]
        public void IsPhoneAwaliableTest()
        {
            User user = new User();
            user.Phone = "+380992525254";
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(user);

            EditController controller = new EditController(userMock.Object, securityMock.Object, imageMock.Object);

            // Act
            var result = controller.IsPhoneAwaliable("+380992525254") as JsonResult;

            // Assert           
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void IsPhoneAwaliableSecondTest()
        {
            User user = new User();
            user.Phone = "14";
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(user);
            userMock.Setup(a => a.IsPhoneExist(It.IsAny<string>())).Returns(true);

            EditController controller = new EditController(userMock.Object, securityMock.Object, imageMock.Object);

            // Act
            var result = controller.IsPhoneAwaliable("145285854555") as JsonResult;

            // Assert           
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void IsPhoneAwaliableThirdTest()
        {
            User user = new User();
            user.Phone = "1";
            userMock.Setup(a => a.GetUserById(It.IsAny<int>())).Returns(user);
            userMock.Setup(a => a.IsPhoneExist(It.IsAny<string>())).Returns(false);

            EditController controller = new EditController(userMock.Object, securityMock.Object, imageMock.Object);

            // Act
            var result = controller.IsPhoneAwaliable("123456789123") as JsonResult;

            // Assert           
            Assert.IsNotNull(result);
        }
    }
}
