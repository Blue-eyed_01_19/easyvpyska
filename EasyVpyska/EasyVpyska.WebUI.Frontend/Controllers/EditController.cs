﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Drawing;

using EasyVpyska.Entities;
using EasyVpyska.WebUI.Abstract;
using EasyVpyska.Repositories.Abstract;

namespace EasyVpyska.WebUI.Frontend.Controllers
{
    public class EditController : Controller
    {
        #region Fields
        private readonly IImageService _imageService;
        private readonly IUserRepository _userRepository;
        private readonly ISecurityManager _securityManager;
        #endregion

        #region Constructor
        public EditController(IUserRepository userRepository, ISecurityManager securityManager,
            IImageService imageService)
        {
            _securityManager = securityManager;
            _userRepository = userRepository;
            _imageService = imageService;
        }
        #endregion

        #region Methods
        //
        // GET: /Edit/
        [Authorize(Roles = "admin, user")]
        [HttpGet]
        [Route("AdDetails/{id?}")]
        public ActionResult Index(string id)
        {
            int currentId;

            if (!int.TryParse(id, out currentId))
            {
                throw new ArgumentException("Should be an integer varialbe");
            }
            
            var user = _userRepository.GetUserById(currentId);
            var image = _userRepository.LoadPhoto(user.Id);

            if (image.File != null)
            {
                var base64Image = "data:image/jpg;base64," + Convert.ToBase64String(image.File);
                ViewBag.Image = base64Image;
            }

            ViewBag.User = user;

            return View();
        }

        [HttpPost]
        public ActionResult Update(string userJson, string password,
            string passwordRepeat, HttpPostedFileBase photo, string cropJson)
        {
            #region Validation

            if (userJson == null)
            {
                throw new ArgumentNullException("user", "can`t be null");
            }

            #endregion

            JavaScriptSerializer _jsonSerializer = new JavaScriptSerializer();
            var user = _jsonSerializer.Deserialize<User>(userJson);
            var crop = _jsonSerializer.Deserialize<ImageCrop>(cropJson);

            user.Status = true;
            user.Role = "user";
            user.Id = -1;

            var violations = CheckNewUserRules(user, password, passwordRepeat);
            CheckImageRules(photo, "photo", violations);
            if (violations.Count == 0)
            {
                user.Id = _securityManager.GetCurrentUserId();
                _imageService.UpdatePhoto(photo, user.Id, crop);
                _userRepository.UpdateUser(user, password);
            }

            var data = new { userId = user.Id, violations = violations };

            var serializator =
                new System.Web.Script.Serialization.JavaScriptSerializer();

            string dataJson = serializator.Serialize(data);

            return Content(dataJson, "application/json");
        }

        #endregion

        #region Helpers

        #region Validation helpers

        [HttpPost]
        public JsonResult IsEmailAwaliable(string email)
        {
            #region Validation
            if (email == null)
            {
                throw new ArgumentNullException();
            }
            #endregion

            int id = _securityManager.GetCurrentUserId();

            if (_userRepository.GetUserById(id).Email == email)
            {
                return new JsonResult { Data = true };
            }

            if (_userRepository.IsEmailExist(email))
            {
                return new JsonResult { Data = false };
            }

            return new JsonResult { Data = true };
        }

        [HttpPost]
        public JsonResult IsPhoneAwaliable(string phone)
        {
            #region Validation
            if (phone == null)
            {
                throw new ArgumentNullException();
            }
            #endregion

            int id = _securityManager.GetCurrentUserId();

            if (_userRepository.GetUserById(id).Phone == phone)
            {
                return new JsonResult { Data = true };
            }

            if (_userRepository.IsPhoneExist(phone))
            {
                return new JsonResult { Data = false };
            }

            return new JsonResult { Data = true };
        }

        private IDictionary<string, string> CheckNewUserRules(User user,
            string password, string passwordRepeat)
        {
            IDictionary<string, string> violations = new Dictionary<string, string>();

            CheckWordRules(user.FirstName, "name", violations);
            CheckWordRules(user.SurName, "surname", violations);
            //CheckWordRules(user.Country, "country", violations);
            //CheckWordRules(user.Town, "city", violations);
            //CheckWordRules(user.Address, "address", violations);

            CheckAddressRules(user, "address", violations);


            CheckEmailRules(user.Email, "email", violations);
            CheckPhoneRules(user.Phone, "phone", violations);

            CheckDateRules(user.DateOfBirth, "date_of_birth", violations);

            CheckPasswordRules(password, "password", violations, 8, 16);
            CheckPasswordRules(passwordRepeat, "passwordRepeat", violations, 8, 16);

            CheckPasswordSimilarity(password, passwordRepeat, "passwordRepeat",
                violations);

            CheckTextLength(user.About, "desctiption", violations, 256);
            CheckTextLength(user.Town, "address", violations, 256);
            CheckTextLength(user.Country, "address", violations, 256);

            return violations;
        }

        private void CheckImageRules(HttpPostedFileBase image, string fieldName, IDictionary<string, string> violations)
        {
            if (image != null
                && image.InputStream.Length >= 3000000)
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please use smaller image"));
            }
        }

        private void CheckWordRules(string word, string fieldName,
            IDictionary<string, string> violations,
            int minLength = 3,
            int maxLength = 64)
        {
            if (string.IsNullOrEmpty(word)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }
            if (word != null
                && word.Length < minLength
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter at least " + minLength.ToString() +
                    " 3 characters."));
            }
            if (word != null
                && word.Length > maxLength
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter no more than " + maxLength.ToString() +
                    " characters."));
            }
        }

        private void CheckEmailRules(string email, string fieldName,
            IDictionary<string, string> violations)
        {
            var regex = new Regex("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+"
                + "(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

            if (string.IsNullOrEmpty(email)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }
            if (email != null
                && !regex.IsMatch(email)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter a valid email address."));
            }
        }

        private void CheckPasswordSimilarity(string password,
            string passwordRepeat,
            string fieldName,
            IDictionary<string, string> violations)
        {
            if (password != null
                && passwordRepeat != null
                && !string.Equals(password, passwordRepeat)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter the same value again."));
            }
        }

        private void CheckAddressRules(User user, string fieldName,
            IDictionary<string, string> violations)
        {
            var originalUser  = _userRepository.GetUserById(_securityManager.GetCurrentUserId());

            var regex = new Regex("^[a-zA-Z]+$");

            if (string.IsNullOrEmpty(user.Country)
                && !violations.ContainsKey(fieldName)
                && originalUser.Country == null)
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter country name please."));
            }

            if (string.IsNullOrEmpty(user.Town)
                && !violations.ContainsKey(fieldName)
                && originalUser.Town == null)
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter town name please."));
            }
        }

        private void CheckPasswordRules(string password, string fieldName,
            IDictionary<string, string> violations,
            int minLength,
            int maxLength)
        {
            var regex = new Regex("^[a-zA-Z0-9]{8,16}$");

            if (password != null
                && password != ""
                && password.Length < minLength
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter at least " + minLength.ToString() +
                    " 3 characters."));
            }
            if (password != null
                && password != ""
                && password.Length > maxLength
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Please enter no more than " + maxLength.ToString() +
                    " characters."));
            }
            if (password != null
                && password != ""
                && !regex.IsMatch(password)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter a valid password"));
            }

        }

        private void CheckDateRules(DateTime dateOfBirth, string fieldName,
            IDictionary<string, string> violations
            )
        {
            if (dateOfBirth == null
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }
            if (dateOfBirth != null
                && GetAge(dateOfBirth) >= 130
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter correct date. You should be younger"));
            }
            if (dateOfBirth != null
                && GetAge(dateOfBirth) < 5
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter correct date. You should be older"));
            }
        }

        private void CheckTextLength(string about, string fieldName,
            IDictionary<string, string> violations,
            int maxLength)
        {
            if (about != null
                && about.Length >= 256
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                     "Please enter no more than " + maxLength.ToString() +
                    " characters."));
            }
        }

        private void CheckPhoneRules(string phone, string fieldName,
            IDictionary<string, string> violations)
        {
            var regex = new Regex("^\\+[0-9]{12}$");

            if (string.IsNullOrEmpty(phone)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter value please."));
            }

            bool res = regex.IsMatch(phone);
            if (phone != null
                && phone != ""
                && !regex.IsMatch(phone)
                && !violations.ContainsKey(fieldName))
            {
                violations.Add(new KeyValuePair<string, string>(fieldName,
                    "Enter valid phone number like +380000000000"));
            }
        }

        private int GetAge(DateTime dateOfBirth)
        {
            return (DateTime.Today.Year - dateOfBirth.Year);
        }

        #endregion

        #endregion
	}
}