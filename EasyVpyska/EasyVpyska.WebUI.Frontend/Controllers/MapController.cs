﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using EasyVpyska.Entities;
using EasyVpyska.Repositories.Abstract;
using EasyVpyska.Repositories.Sql;

using EasyVpyska.WebUI.Frontend.Models;
 

namespace EasyVpyska.WebUI.Frontend.Controllers
{
          
    public class MapController : Controller
    {
        #region Fields
        private readonly IAdRepository _adRepository;
        #endregion

        #region Constructor
        public MapController(IAdRepository adRepository)
        {
            _adRepository = adRepository;
        }
        #endregion

        #region Methods
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetAddresses()
        {
            List<Ad> adsList = _adRepository.GetAllAdsByType(false);

            List<AdAddressModel> addresses = new List<AdAddressModel>();

            adsList.ForEach(ad => addresses.Add(new AdAddressModel() { Address = ad.Town + " " + ad.Address, Id = ad.Id }));

            return Json(new
            {
                startPosition = new { latitude = 49.6594482, longitude = 31.5560814, zoom = 6 },
                addresses
            });
        }

        [HttpPost]
        public ActionResult GetAdFullInfo(int Id)
        {
            var Ad = _adRepository.GetAd(Id);         

            string name = "";           
            string description = "";
            string from = "";
            string to = "";
            string adLink = "";


            if (Ad != null)
            {
                description = Ad.Description;
                from = Ad.BeginTime.ToShortDateString();
                to = Ad.EndTime.ToShortDateString();
                adLink = Ad.Id.ToString();
            }

            if (Ad.Author.FirstName != null)
            {
                name = Ad.Author.FirstName;                 
                
            }

            return Json(new
            {
                name = name,                           
                from = from,
                to = to,
                description = description,
                adLink = adLink
            });
        }
        #endregion
    }
}