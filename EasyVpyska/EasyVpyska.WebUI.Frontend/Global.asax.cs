﻿using EasyVpyska.WebUI.Frontend.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using System.Web.Security;
using EasyVpyska.WebUI.Frontend.Controllers;
using EasyVpyska.WebUI.Abstract;
using NLog;

namespace EasyVpyska.WebUI.Frontend
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_AuthenticateRequest(
           object sender,
           EventArgs e
        )
        {

            ISecurityManager _securityManager = DependencyResolver.Current.GetService<ISecurityManager>();
            if (_securityManager.IsCurrentUserAuthenticated())
            {
                string userName = _securityManager.GetCurrentUserName();
                if (!_securityManager.IsUserEnabled(userName))
                {
                    _securityManager.SignOut();
                }
            }
        }
        

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                if (Server.GetLastError() != null)
                {
                    Exception ex = Server.GetLastError().GetBaseException();

                    if (ex is HttpException == false)
                    {
                        Logger logger = LogManager.GetCurrentClassLogger();
                        logger.Log(LogLevel.Error, ex);
                    }                   
                }
            }
            catch
            { }
        }

        protected void Application_EndRequest()
        {
            if (Context.Response.StatusCode == 404)
            {
                Response.Clear();

                var rd = new RouteData();
                 // In case controller is in another area
                rd.Values["controller"] = "Errors";
                rd.Values["action"] = "Error404";
                RequestService r = new RequestService();

                IController c = new ErrorsController(r);
                c.Execute(new RequestContext(new HttpContextWrapper(Context), rd));
            }


            if (Context.Response.StatusCode == 500)
            {
                Response.Clear();

                var rd = new RouteData();
                // In case controller is in another area
                rd.Values["controller"] = "Errors";
                rd.Values["action"] = "Error500";
                RequestService r = new RequestService();

                IController c = new ErrorsController(r);
                c.Execute(new RequestContext(new HttpContextWrapper(Context), rd));
            }
        }
    }
}
