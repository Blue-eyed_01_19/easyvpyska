﻿(function ($) {

    var AdDetailsPage = function () {

        var that = this;
        

        this.initialize = function () {
            console.log("[map-page] initialize", arguments);
            this.adId = $("#Id").val();

            this.$addCommentButton = $(".new-comment .add-comment-button");
            this.$addCommentText = $(".new-comment .new-comment-text");
            this.$addCommentValidation = $(".new-comment .validation");
            this.$SubscribeButton = $("#subs");


            this.$addCommentButton.on("click", this.onAddComment);
            this.$SubscribeButton.on("click", this.onSubscribe);

            this.$addCommentText.on("keyup change paste", function () {
                console.log("[addetails-page] $addCommentText changed");
                that.validateNewComment();
            });
            google.maps.event.addDomListener(window, 'load', this.mapInitialize);
        };

        this.mapInitialize = function () {
            console.log("[map-page] mapInitialize", arguments, this);

            $.ajax({
                url: "/AdDetails/GetAddresses",
                type: "POST",
                dataType: "json",
                data: {
                    id: $("#Id").val()
                }
            }).done(function (data) {
                that.mapDisplay(data);
                console.log("[map-page] mapInitialize - done", arguments, this);
            });
        };

        this.mapDisplay = function(data) {

            console.log("[map-page] mapDisplay", arguments, this);

            var mapOptions = {
                zoom: data.startPosition.zoom,
                center: new google.maps.LatLng(data.startPosition.latitude, data.startPosition.longitude),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            this.setStyle(map);

            this.setMarkers(map, data.addresses, data.idList);
        };

        this.setStyle = function (map) {
            console.log("[map-page] setStyle", arguments, this);

            var styles = [
                {
                    stylers: [
                        { "invert_lightness": true },
                        { "hue": "#007fff" },
                        { "visibility": "on" }
                    ]
                }
            ];

            var styledMap = new google.maps.StyledMapType(styles,
                    { name: "Styled Map" });
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
        };

        this.setMarkers = function (map, addresses, idList) {
            var geocoder = new google.maps.Geocoder();
           
            
            var infowindow = new google.maps.InfoWindow();

            console.log("[map-page] setMarkers", arguments, this);

            var markerClusters = [];
            var markerArrays = Object();

            for (var i in addresses) {

                console.log("[map-page] geocoding array iteration", i, this);

                doGeocoding(addresses[i]);
            }

            function doGeocoding(addressItem) {
                geocoder.geocode({ 'address': addressItem.Address }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {

                        marker = new google.maps.Marker({ position: results[0].geometry.location });
                        google.maps.event.addListener(marker, 'click', toggleBounce);

                        for (var i = 0; i < results[0].address_components.length; i++) {
                            if (results[0].address_components[i].types[0] == "administrative_area_level_1") {
                                //this is the object for province
                                provnce = results[0].address_components[i]['long_name'];
                            }
                        }

                        marker.setMap(map);

                        map.setCenter(results[0].geometry.location);
                        map.setZoom(15);

                        provnce = provnce.split(" ", 1);

                        marker.Id = addressItem.Id;

                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }
            function toggleBounce() {
                var name = "";                
                var description = "";
                var userLink = "";

                $.ajax({
                    url: "/AdDetails/GetAdInfo",
                    type: "POST",
                    dataType: "json",
                    data: {

                        id: this.Id
                    }
                }).done(function (data) {

                    //set
                    name = data.name;
                                                       
                    description = data.description;
                    userLink = data.userLink;

                    
                    var contentString = '<div class="header"><p class="name">' + name + '</p></div><div id="content">' + description + '</div><p class="link"><a href="/User/Index/' + userLink + '">View profile</a></p>';

                    infowindow.setContent(contentString);


                     console.log(contentString);
                     console.log(description);
                    console.log("[map-page] infowindow - done", arguments, this);
                });

                infowindow.open(map, this);

                if (this.getAnimation() != null) {
                    this.setAnimation(null);
                } else {
                    this.setAnimation(google.maps.Animation.BOUNCE);
                    marker = this;
                    setTimeout(function () { marker.setAnimation(null); }, 3 * 710);
                }
            }
        }
      
      
        this.onAddComment = function () {
            console.log("[addetails-page] onAddComment", arguments);
            
            if (that.validateNewComment() == false) {
                return;
            }

            var comment = that.$addCommentText.val();

            $.ajax({
                url: "/AdDetails/AddComment",
                type: "POST",
                dataType: "json",
                data: {
                    adId: that.adId,                   
                    comment: comment
                }
            }).done(function (data) {
                console.log("[addetails-page] onAddComment - done", arguments);
                if (data.failed) {
                    var validationMessage = (data.validation && data.validation.length > 0
                                                ? data.validation.join("\n")
                                                : "Internal server error occurred");
                    that.setNewCommentValidationMessage(validationMessage);
                } else {
                    that.$addCommentText.val("");
                    that.setNewCommentValidationMessage(undefined);                                  
                    
                    var wrapp = document.getElementById("fresh-comment");
                    var div = document.createElement('div');
                    div.innerHTML = '<div class="old-comments"><p class="comment-author">' + data.userName + '</p><p id="date">' + that.tDate() + '</p><div id="comment">' + comment + '</div></div>';

                    wrapp.insertBefore(div, wrapp.firstChild);         
                }
                
            }).fail(function () {
                console.log("[addetails-page] onAddComment - fail", arguments);
            });
        };

        this.onSubscribe = function () {
            console.log("[addetails-page] onSubscribe", arguments);              

            $.ajax({
                url: "/AdDetails/UserSubscribed",
                type: "POST",
                dataType: "json",
                data: {
                    adId: that.adId                    
                }
            }).done(function (data) {
                console.log("[addetails-page] onSubscribe - done", arguments);            
                var wrapp = document.getElementById("subsRow");
                wrapp.innerHTML = 'You subscribed on this ad';
            }).fail(function () {
                console.log("[addetails-page] onSubscribe - fail", arguments);
            });
        };


        this.checkNewCommentRules = function (comment) {
            var violations = [];
            if (!comment || (comment.length < 1)) {
                violations.push("Comment should not be empty");
            }
            if (comment && comment.length > 500) {
                violations.push("Comment length should not exceed 500 symbols");
            }
            return violations;
        };

        this.validateNewComment = function () {
            var comment = this.$addCommentText.val();

            var violations = this.checkNewCommentRules(comment);
            if (violations && violations.length > 0) {
                var validationMessage = violations.join("\n");
                this.setNewCommentValidationMessage(validationMessage);
                return false;
            } else {
                this.setNewCommentValidationMessage(undefined);
                return true;
            }
        };

        this.setNewCommentValidationMessage = function (validationMessage) {
            if (validationMessage) {
                this.$addCommentValidation.show().text(validationMessage);
                this.$addCommentText.addClass("invalid");
            } else {
                this.$addCommentValidation.hide().text();
                this.$addCommentText.removeClass("invalid");
            }
        };

        this.tDate = function () {
            var D = new Date();

            var Day = D.getDate();
            if (Day < 10) {
                Day = "0" + Day;
            }
            var Month = D.getMonth();
            if (Month < 9) {
                Month = parseInt(Month) + 1;
                Month = "0" + Month;
            }
            else {
                Month = parseInt(Month) + 1;
            }
            var Year = D.getFullYear();
            temp = Day + "." + Month + "." + Year;
            return temp;
        };

    };

    $(document).on("ready", function () {
        console.log("[map-page] has loaded", arguments, this);
        var page = new AdDetailsPage();
        page.initialize();
    });
    
})(window.jQuery);