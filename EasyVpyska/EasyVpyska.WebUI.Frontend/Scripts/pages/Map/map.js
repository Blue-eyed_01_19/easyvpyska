﻿
(function ($) {

    var mapPage = function () {

        this.initialize = function () {
            console.log("[map-page] initialize", arguments);
        };

        this.mapInitialize = function () {
            console.log("[map-page] mapInitialize", arguments, this);

            $.ajax({
                url: "/Map/GetAddresses",
                type: "POST",
                dataType: "json",
            }).done(function (data) {
                mapDisplay(data);
                console.log("[map-page] mapInitialize - done", arguments, this);
            });
        };

        google.maps.event.addDomListener(window, 'load', this.mapInitialize);
    };


    $(function () {
        console.log("[map-page] has loaded", arguments, this);
        var page = new mapPage();
        page.initialize();
    });



    function mapDisplay(data) {

        console.log("[map-page] mapDisplay", arguments, this);

        var mapOptions = {
            zoom: data.startPosition.zoom,
            center: new google.maps.LatLng(data.startPosition.latitude, data.startPosition.longitude),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        setStyle(map);

        setMarkers(map, data.addresses, data.idList);
    };

    function setStyle(map) {
        console.log("[map-page] setStyle", arguments, this);

        var styles = [
            {
                stylers: [
                    { "invert_lightness": true },
                    { "hue": "#039cb7" },
                    { "visibility": "on" }
                ]
            }
        ];

        var styledMap = new google.maps.StyledMapType(styles,
                { name: "Styled Map" });
        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');
    }

    function setMarkers(map, addresses, idList) {
        var geocoder = new google.maps.Geocoder();
        var infowindow = new google.maps.InfoWindow();

        console.log("[map-page] setMarkers", arguments, this);

        var markerClusters = [];
        var markerArrays = Object();

        for (var i in addresses) {

            console.log("[map-page] geocoding array iteration", i, this);

            doTimeout(i);
            
        }

        function doTimeout(i)
        {
            setTimeout(function () {
                doGeocoding(addresses[i]);
            }, i * 200);
        }

        function doGeocoding(addressItem) {
            geocoder.geocode({ 'address': addressItem.Address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    marker = new google.maps.Marker({ position: results[0].geometry.location,
                    animation: google.maps.Animation.DROP});

                    google.maps.event.addListener(marker, 'click', toggleBounce);

                    for (var i = 0; i < results[0].address_components.length; i++) {
                        if (results[0].address_components[i].types[0] == "administrative_area_level_1") {
                            //this is the object for province
                            provnce = results[0].address_components[i]['long_name'];
                        }
                    }
                    provnce = provnce.split(" ", 1);

                    marker.Id = addressItem.Id;

                    if (markerArrays.hasOwnProperty(provnce)) {
                        markerArrays[provnce].push(marker);
                        markerClusters[provnce].addMarker(marker);
                    }
                    else {
                        markerArrays[provnce] = new Array();
                        markerArrays[provnce].push(marker);

                        var mcOptions = { gridSize: 50, maxZoom: 10 };
                        markerClusters[provnce] = new MarkerClusterer(map, [], mcOptions);
                        markerClusters[provnce].addMarker(marker);
                    }

                } else {
                    console.log('Geocode was not successful for the following reason: ' + status)
                }
            });
        }
        function toggleBounce() {
            var name = "";
            var from = "";
            var to = "";
            var description = "";
            var adLink = "";

            $.ajax({
                url: "/Map/GetAdFullInfo",
                type: "POST",
                dataType: "json",
                data: {
                    id: this.Id
                }
            }).done(function (data) {

                name = data.name;
                from = data.from;
                to = data.to;
                description = data.description;
                adLink = data.adLink;


                var contentString = '<div class="header"><p class="name">' + name + '</p></div><div class="date"><p>From:' + from + '</p><p>To:' + to + '</p></div><div id="content">' + description + '</div><p class="link"><a href="/AdDetails/Index/' + adLink + '">View details</a></p>';

                infowindow.setContent(contentString);
               
                
                console.log("[map-page] infowindow - done", arguments, this);
            });

            infowindow.open(map, this);

            if (this.getAnimation() != null) {
                this.setAnimation(null);
            } else {
                this.setAnimation(google.maps.Animation.BOUNCE);
                marker = this;
                setTimeout(function () { marker.setAnimation(null); }, 3 * 710);
            }
        }
    }
        
})(window.jQuery);







