﻿using EasyVpyska.WebUI.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EasyVpyska.WebUI
{
    public class EmailService : IEmailService
    {
        #region IEmailService

        public void SendEmail(string to, string subject, string body)
        {
            using (SmtpClient client = new SmtpClient())
            {
                MailMessage message = new MailMessage();
                message.To.Add(to);
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = true;
                client.Send(message);
            }
        }

        #endregion
    }
}
